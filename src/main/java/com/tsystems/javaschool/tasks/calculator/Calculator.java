package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Scanner;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public static String evaluateListOfPostfix(List<String> postfix) {
        Deque<Double> stack = new ArrayDeque<Double>();

        if (postfix.isEmpty()){
            return "";
        }
        for (String x : postfix) {

            if (x.equals("+")) stack.push(stack.pop() + stack.pop());
            else if (x.equals("-")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a - b);
            }
            else if (x.equals("*")) stack.push(stack.pop() * stack.pop());
            else if (x.equals("/")) {
                Double b = stack.pop(), a = stack.pop();
                stack.push(a / b);
            }            else if (x.equals("u-")) stack.push(-stack.pop());
            else stack.push(Double.valueOf(x));
        }

        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);

        Double d = stack.pop().doubleValue();
        return df.format(d);
    }

    public String evaluate(String statement) {
        if (statement == null) return null;

        RPNParser parser = new RPNParser();
        List<String> expression = parser.parse(statement);

        String result = evaluateListOfPostfix(expression);
        System.out.println("result: " + result);
        if (result.equals("") || result.equals("∞"))
            return null;
        return result.replace(",", ".");
    }

}
