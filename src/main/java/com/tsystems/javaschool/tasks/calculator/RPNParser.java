package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class RPNParser {
    private static String operators = "+-*/";
    private static String delimiters = "() " + operators;
    public static boolean flag = true;

    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;

        for (int i = 0; i < delimiters.length(); i++) {
            if (token.charAt(0) == delimiters.charAt(i)) return true;
        }
        return false;
    }

    private static boolean isOperator(String token) {
        //if (token.equals("u-")) return true;
        for (int i = 0; i < operators.length(); i++) {
            if (token.charAt(0) == operators.charAt(i)) return true;
        }
        return false;
    }

    private static int GetPriority(String s)
    {
        switch (s)
        {
            case "(": return 0;
            case "+": return 2;
            case "-": return 2;
            case "*": return 3;
            case "/": return 3;
            default: return 4;
        }
    }

    public static List<String> parse (String infix) {
        List<String> postfix = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();

        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);

        String prev = "";
        String curr = "";

        if (tokenizer.hasMoreTokens()){
            curr = tokenizer.nextToken();

            if (isOperator(curr) || curr.equals(")")){
                System.out.println("Некорректное выражение.");
                flag = false;
                postfix.clear();
                return postfix;
            }

            if (isDelimiter(curr)){
                if (curr.equals("(")){
                    stack.push(curr);
                } else{
                    /*if (curr.equals("-")) {
                        // унарный минус
                        curr = "u-";

                    } else{*/
                        while (!stack.isEmpty() && (GetPriority(curr) <= GetPriority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }
                   // }
                    if (!curr.equals(" ")) {
                        stack.push(curr);
                    }
                }
            }

            else if (curr.matches("-?\\d+(\\.\\d+)?")){
                postfix.add(curr);
            }else {
                System.out.println("Некорректное выражение.");
                flag = false;
                postfix.clear();
                return postfix;
            }
            prev = curr;
        }

        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();

            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                System.out.println("Некорректное выражение.");
                flag = false;
                postfix.clear();
                return postfix;
            }

            if (curr.equals(" ")) continue;
            if (isDelimiter(curr)){

                if (curr.equals("(")){
                    stack.push(curr);
                } else if (curr.equals(")")) {

                    while (!stack.peek().equals("(")) {
                        postfix.add(stack.pop());
                        if (stack.isEmpty()) {
                            System.out.println("Скобки не согласованы.");
                            flag = false;
                            postfix.clear();
                            return postfix;
                        }
                    }
                    stack.pop();
                } else{
                  /*  if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev)  && !prev.equals(")")))) {
                        // унарный минус
                        curr = "u-";

                    } else{*/
                        while (!stack.isEmpty() && (GetPriority(curr) <= GetPriority(stack.peek()))) {
                            postfix.add(stack.pop());
                        }
                   // }
                    if (isOperator(prev)) {
                        postfix.clear();
                        return postfix;
                    }else stack.push(curr);
                }
            }

            else if (curr.matches("-?\\d+(\\.\\d+)?")){
                postfix.add(curr);
            }else {
                System.out.println("Некорректное выражение.");
                flag = false;
                postfix.clear();
                return postfix;
            }
            prev = curr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                System.out.println("Скобки не согласованы.");
                flag = false;
                postfix.clear();
                return postfix;
            }
        }
        return postfix;
    }
}
