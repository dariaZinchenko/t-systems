package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    private int size;
    public int getSize(){return size;}

    public CannotBuildPyramidException(String message, int size){
        super(message);
        this.size = size;
    }
}
