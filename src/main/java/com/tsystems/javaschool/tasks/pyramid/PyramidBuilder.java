package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int row = 0;
        int column = 0;

        if (inputNumbers.contains(null)){
            throw new CannotBuildPyramidException("Array contains null", inputNumbers.size());
        } else if (IsPyramid(inputNumbers)) {
            int d = 1 + 8 * inputNumbers.size();
            row = (int) (Math.sqrt(d) - 1) / 2;
            column = 2 * row - 1;
            Collections.sort(inputNumbers);
        }

        int[][] PyramidArr = new int[row][column];

        int array_index = 0; // счетчик для массива
        int j = 1; // количество символов в пирамиде не считая нулей в начале и в конце
        boolean is_zero = false; //

        for (int i = 0; i < row; i++) {
            int zeros = (column - j) / 2; // количество нулей в начале и в конце строки
            for (int k = 0; k < column; k++) {
                if (k < zeros) PyramidArr[i][k] = 0;
                else if (k < j + zeros) {
                    if (is_zero) {
                        PyramidArr[i][k] = 0;
                        is_zero = !is_zero;
                    } else {
                        PyramidArr[i][k] = inputNumbers.get(array_index);
                        array_index++;
                        is_zero = !is_zero;
                    }
                } else if (k >= j + zeros) PyramidArr[i][k] = 0;
            }
            is_zero = false;
            j += 2;
        }
        return PyramidArr;
    }

    public static boolean IsPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        //можно построить пирамиду если длинна массива равна частичной сумме натурального ряда
        int d = 1 + 8 * inputNumbers.size(); // => нужно решить уравнение(k(k+1))/2=length => k^2 + k - 2*length = 0
        double x = (Math.sqrt(d) - 1) / 2;

        if (x % 1 == 0) {  // если корень не целый значит число не является част суммой => треугольник построить невозможно
            return true;
        } else {
            throw new CannotBuildPyramidException("It is impossible to build a pyramid. Invalid array size", inputNumbers.size());
        }
    }
}
